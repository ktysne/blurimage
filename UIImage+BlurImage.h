//
//  UIImage+BlurImage.h
//  FastPhotoTweet
//
//  Created by ktysne on 2014/07/01.
//
//

@interface UIImage (BlurImage)

+ (UIImage *)blurImageWithView:(UIView *)view;
+ (UIImage *)blurImageWithView:(UIView *)view radius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

+ (UIImage *)blurImageWithKeyWindow;
+ (UIImage *)blurImageWithWindow:(UIWindow *)window;
+ (UIImage *)blurImageWithWindow:(UIWindow *)window radius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

- (UIImage *)applyBlurEffect;
- (UIImage *)applyBlurEffectWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

@end
